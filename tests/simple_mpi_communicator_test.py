#!/usr/bin/env python3
import base64
import sys
import os
import json
from yggdrasil.erebor   import runner
from yggdrasil.erebor   import FrameworkControler
from yggdrasil import consts

class MPIComTest(FrameworkControler):

    def __init__(self, erebor, ID, node_list, tfile):
        FrameworkControler.__init__(self, erebor, ID, tfile)
        self.node_list = "A,A,A,A"
        self.groups    = dict()

    def dead_nodes(self, rank, ID, nodes) :
        print("dead nodes [ from rank {} ID {}] {}".format(rank, ID, nodes))

    def network_up(self, ID) :
        self.to_wait_groups -= 1
        self.to_wait_pings   = len(self.groups) * len(self.groups)
        print("network UP {} rest to wait {}".format(ID,
            self.to_wait_groups))
        self.log_time("new group is UP")
        if self.to_wait_groups == 0 :
            self.log_time("all groups launched")
            self.erebor.terminate()
            self.close()

    def start(self, networkId):
        print("\n 'connected' \n")
        network = self.erebor.networks.get(networkId)
        self.log_time("taktuk connected")
        # get notified on dead nodes
        self.erebor.on_dead_nodes(self.dead_nodes)

        port_g1 = "6000"

        def group_g1_up(ID):
            print("group g1 ready")
            # make it start a mpi session
            def mpi_session_started(data) :
                print("executing a simple command")
                command = os.environ["PWD"]+"/tests/simple_mpi_emulator.py -r 1 -p 6000 -n g1"
                self.exec_on("0", command, consts.TRUE, "0", "g1", "0", "root",
                        self.job_callback)
            self.start_mpi_session("g1", "0", "root", port_g1,
                                    mpi_session_started)
        # start group g1
        self.erebor.on_network_init("g1", group_g1_up)
        self.new_group_on("g1", "", "0", "root", "0", "root")

    def job_callback(self, data) :
        decoded = json.loads(data)
        value = decoded["stderr"]
        for line in value :
            line = base64.b64decode(line).decode(consts.encoding)
            line = line.split("error >")[1]
            print ("{} : {}".format("stderr", line))
        value = decoded["stdout"]
        for line in value :
            line = base64.b64decode(line).decode(consts.encoding)
            line = line.split("output >")[1]
            print ("{} : {}".format("stdout", line))
        print ("{} : {}".format("status",
            base64.b64decode(decoded["status"]).decode(consts.encoding)))
        self.erebor.terminate()
        self.close()

if __name__ == "__main__":
    sys.exit(runner(sys.argv, MPIComTest))
