#!/usr/bin/env python3
import sys
from yggdrasil import consts
from yggdrasil.erebor   import runner
from yggdrasil.task_lib import Group
from yggdrasil.task_lib import NumberedGroup
from yggdrasil.task_lib import Broadcaster
from yggdrasil.task_lib import SerialBroadcaster
from yggdrasil.task_lib import ParrallelBroadcaster
from yggdrasil.task_lib import Ventilator
from yggdrasil.task_lib import TaskProcessor

class Sample(TaskProcessor):
    def __init__(self, erebor, ID, node_list, tfile=None):
        TaskProcessor.__init__(self, erebor, ID, tfile)

    def register_tasks(self) :
        g1 = Group("g1", "g1", "A,B", "0", "root")
        g2 = NumberedGroup("g2", "g2", "A,A,A,A", "0", "root")
        b1 = Broadcaster("b1", "uptime", g1)
        b1.add_dependency_conjunction({g1:consts.RUNING}, True)
        # serial broadcast after b1 on g1
        b2 = SerialBroadcaster("b2", ["pouet", "pwd"], g1)
        b2.add_dependency_conjunction({b1:consts.RUNING}, True)
        # parallel broadcast after b2 on g1
        b3 = ParrallelBroadcaster("b3", ["df", "uname -a", "uptime", "uptime"], g1 ,True)
        b3.add_dependency_conjunction({b2:consts.DONE}, True)
        b3.add_dependency_conjunction({b2:consts.ERROR}, True)
        # ventilator while b3 on g2
        v1 = Ventilator("v1", ["uptime", "uptime", "uptime", "uptime", "uptime",
                        "uptime", "uptime", "uptime", "nono"], g2,True, -1, 1)
        v1.add_dependency_conjunction({b3:consts.RUNING, g2:consts.RUNING}, True)
        # On a new group g3
        g3 = NumberedGroup("g3", "g3", "A", "0", "root")
        # broadcast that can timeout after v1 done
        b4 = Broadcaster("b4", "sleep 2", g3, False, 1)
        b4.add_dependency_conjunction({v1:consts.DONE,g3:consts.RUNING}, True)
        # ventilator executed if b4 timeout, canceled otherwise
        v2 = Ventilator("v2", ["echo timeout"], g3, False)
        v2.add_dependency_conjunction({b4:consts.TIMEOUT}, True)
        v2.add_dependency_conjunction({b4:consts.DONE}, False)
        # ventilator executed if b4 is done, canceled otherwise
        v3 = Ventilator("v3", ["echo pas de timeout"], g3, False)
        v3.add_dependency_conjunction({b4:consts.DONE}, True)
        v3.add_dependency_conjunction({b4:consts.TIMEOUT}, False)
        # g3 canceled, after done of v2 or v3 (way to reduce)
        g3.add_dependency_conjunction({v2:consts.DONE}, False)
        g3.add_dependency_conjunction({v3:consts.DONE}, False)
        # append all tasks to the list
        self.tasks.append(g1)
        self.tasks.append(g2)
        self.tasks.append(b1)
        self.tasks.append(b2)
        self.tasks.append(b3)
        self.tasks.append(v1)
        self.tasks.append(g3)
        self.tasks.append(b4)
        self.tasks.append(v2)
        self.tasks.append(v3)

if __name__ == "__main__":
    sys.exit(runner(sys.argv, Sample))
