# -*- mode: ruby -*-
# vi: set ft=ruby :

$bootstrap = <<SCRIPT
# install core dependencies
apt-get update
apt-get install -y taktuk python3 python3-zmq python3-pexpect python3-setuptools python3-pip

# install main repo
cd /yggdrasil/yggdrasil.*
pip3 install -e .

# user libraries
cd /yggdrasil/yggdrasil-c.*/c_wrapper
cp ./install/include/* /usr/include/
cp ./install/lib/* /usr/lib/

# etc/hosts
echo "127.0.0.1 A" >> /etc/hosts
echo "127.0.0.1 B" >> /etc/hosts
echo "127.0.0.1 C" >> /etc/hosts
echo "127.0.0.1 D" >> /etc/hosts
echo "127.0.0.1 E" >> /etc/hosts
echo "127.0.0.1 F" >> /etc/hosts
echo "127.0.0.1 G" >> /etc/hosts
echo "127.0.0.1 H" >> /etc/hosts
echo "127.0.0.1 I" >> /etc/hosts
echo "127.0.0.1 J" >> /etc/hosts
SCRIPT

$userconfig = <<SCRIPT
# ssh config
cd /yggdrasil/yggdrasil-integration.*
cat ssh/id_rsa.pub >> ~/.ssh/authorized_keys
cp ssh/id_rsa* ~/.ssh/
cp ssh/config ~/.ssh/
chmod 600 ~/.ssh/id_rsa
SCRIPT

VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "ubuntu/xenial64"
  config.vm.synced_folder "../", "/yggdrasil"
  config.vm.hostname = "A"
  config.vm.define "A" do |node|
	  node.vm.network "private_network", ip: "192.168.0.10"
	  node.vm.provision :shell, inline: $bootstrap
	  node.vm.provision :shell, privileged: false, inline: $userconfig
  end
end
