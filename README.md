# Installation

Install the package yggdrasil :

```
git clone git@gitlab.com:lavocat/yggdrasil.git
cd yggdrasil
pip install -e .
```

Get the yggdrasil-c library :


```
git clone git@gitlab.com:lavocat/yggdrasil-c.git
cd yggdrasil-c
cat README
```

Then :

1. Follow instructions in its README to build the c_wrapper library.
2. Link built client and server files to the empty c_wrapper folder
3. Apply the following ssh configuration
4. Launch tests

## SSH config file containing

```
Host A
    HostName        A
    IdentityFile    /home/$USERNAME$/.ssh/passord_less_key
Host B
    HostName        B
    IdentityFile    /home/$USERNAME$/.ssh/passord_less_key
Host C
    HostName        C
    IdentityFile    /home/$USERNAME$/.ssh/passord_less_key
Host D
    HostName        D
    IdentityFile    /home/$USERNAME$/.ssh/passord_less_key
Host E
    HostName        E
    IdentityFile    /home/$USERNAME$/.ssh/passord_less_key
Host F
    HostName        F
    IdentityFile    /home/$USERNAME$/.ssh/passord_less_key
```

Replacing $USERNAME$ by your user name.

## /etc/hosts containing

```
127.0.1.1	A
127.0.1.1	B
127.0.1.1	C
127.0.1.1	D
127.0.1.1	E
127.0.1.1	F
127.0.1.1	G
127.0.1.1	H
127.0.1.1	I
127.0.1.1	J
127.0.1.1	K
127.0.1.1	L
127.0.1.1	M
```
### SSHD config

Ensure no max session is set.

## Testing installation

If everything is done properly, you should be able to run the `./regression.sh`
script in the python folder. If not, and if you are sure of your configuration,
try to run each example one after the other to see what is wrong and consider
adding an issue in the bug tracker.

# Testing

```
./regression.sh
```
